(function () {
   console.log('--JS--');

   $("button[data-add-product]").click(function () {
      let self = this;
      let productId = self.getAttribute('data-add-product');
      $(self).removeClass('btn-primary').addClass('btn-secondary').prop('disabled', true);
      $.post( "/shop/add-to-cart", {productId: productId}, function( data ) {
         if(!data)
             $(self).removeClass('btn-secondary').addClass('btn-primary').prop('disabled', true);
      }, "json");
   });

   function countTotalPrice(){
      let totalPrice = 0;
      $("form .cart-product").each(function (){
         totalPrice += parseInt($("b.price", this).text()) * parseInt($(".input-number", this).val());
      });
      $("form b.total-price").text(totalPrice);
   }
   countTotalPrice();

   $("form .input-number").change(function (){
      countTotalPrice();
   });

   $("form .cart-product-delete").click(function () {
      let card = $(this).closest('.cart-product');
      let id = $("input[name^='product']", card).val();

      let orderId = $("form input[name='order']").val();

      $.post( "/shop/delete-product", {productId: id, orderId: orderId}, function( data ) {
         if(data) {
            card.closest('.card').remove();
            countTotalPrice();
         }

         if(!$('.cart-list .card').length){
            $('.cart-list').html('<h2>CART EMPTY</h2>');
         }
      }, "json");
   });
})()
