<?php

namespace app\controllers;

use app\models\Cart;
use app\models\Order;
use app\models\Product;
use app\models\User;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Shop;

use function Symfony\Component\DomCrawler\Test\Constraint\toString;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        User::cookieUserId();
        return $this->render('index');
    }

    /**
     * Displays homepage.
     *
     * @param int $id
     * @return string
     */
    public function actionShop($id = 1): string
    {
        $userId = User::cookieUserId();

        $shops = Shop::find()
            ->asArray()
            ->all();

        $products = Product::find()
            ->where(['shopid' => $id])
            ->asArray()
            ->all();

        $order = Order::findOne(['userid' => $userId, 'status' => Order::ORDER_STATUS_START]);
        $cart =  Cart::find()->select(['productid'])->where(['orderid' => $order->id ?? 0])->asArray()->column();

        return $this->render('shop', [
            'shops' => $shops,
            'products' => $products,
            'shopId' => $id,
            'cart' => $cart
        ]);
    }

    public function actionShoppingCart(): string
    {
        $userId = User::cookieUserId();

        $order = Order::findOne(['userid'=>$userId, 'status'=>Order::ORDER_STATUS_START]);

        $user = User::findOne(['id' => $userId]);

        $products = [];
        $orderid = null;
        $productQuantity = [];

        if($order){
            $cart = Cart::find()->select(['productid', 'quantity'])->where(['orderid'=>$order->id])->asArray()->all();

            $p = [];

            foreach ($cart as $c) {
                array_push($p, $c['productid']);
                $productQuantity[$c['productid']] = $c['quantity'];
            }

            $orderid = $order->id;
            $products = Product::find()->where(['id'=>$p])->asArray()->all();
        }

        return $this->render('shopping-cart', [
            'products'=>$products,
            'order'=>$orderid,
            'productQuantity'=>$productQuantity,
            'user'=>$user
        ]);
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionDone()
    {
        return $this->render('done');
    }
}
