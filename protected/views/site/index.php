<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?= Html::a(
            Html::img('@web/images/courier-1920.jpg', ['alt' => 'Courier', 'class'=>'img-fluid']),
            ['site/shop'],
            ['class' => 'text-decoration-none']);
    ?>
</div>
