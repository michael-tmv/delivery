<?php

/** @var yii\web\View $this */


use yii\bootstrap5\ActiveForm;

$this->title = 'Shopping Cart';
$this->params['breadcrumbs'][] = $this->title;
$n = 1;
?>
<div class="site-shop">
    <?php $form = ActiveForm::begin(['id' => 'cart-form', 'action' => '/order/buy']); ?>
        <div class="row">
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <input type="hidden" name="order" class="form-control" value="<?=$order?>">

                        <label for="name" class="form-label">Name</label>
                        <input type="text" class="form-control" name="name" id="name" value="<?=$user->username;?>" required aria-describedby="nameHelp">

                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-control" name="email" id="email" value="<?=$user->email;?>" required aria-describedby="emailHelp">

                        <label for="phone" class="form-label">Phone</label>
                        <input type="text" class="form-control" name="phone" id="phone" value="<?=$user->phone;?>" required aria-describedby="phoneHelp">

                        <label for="address" class="form-label">Address</label>
                        <input type="text" class="form-control" name="address" id="address" value="<?=$user->address;?>" required aria-describedby="addressHelp">
                    </div>
                </div>
            </div>
            <div class="col-8 cart-list">
                <?if(count($products)):?>
                    <?foreach ($products as $p):?>
                    <div class="card">
                        <div class="row g-0">
                            <div class="col-md-4">
                                <img src="/images/p<?=$p['image']?>.png" class="img-fluid rounded-start" alt="<?=$p['name']?>">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body cart-product">
                                    <h5 class="card-title"><?=$p['name']?></h5>
                                    <p class="card-text"><?=str_replace("\r\n",'<br>',$p['description'])?></p>
                                    <input type="hidden" name="product[<?=$p['id']?>]" class="form-control" value="<?=$p['id']?>">
                                    <div class="row g-0">
                                        <div class="col-6">
                                            Price: <b class="price"><?=$p['price']?></b>
                                        </div>
                                        <div class="col-4">
                                            <input type="number" name="quantity[<?=$p['id']?>]" class="form-control input-number" value="<?=$productQuantity[$p['id']]?>" min="1">
                                        </div>
                                        <div class="col-2 text-end">
                                            <button class="btn btn-secondary cart-product-delete"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                                    <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                                </svg></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <?endforeach;?>
                    <div class="row">
                        <div class="col-6">
                            <p>Total price: <b class="total-price"></b></p>
                        </div>
                        <div class="col-6">
                            <button style="width:100%;" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                <?else:?>
                    <h2>CART EMPTY</h2>
                <?endif;?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
