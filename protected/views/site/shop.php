<?php

/** @var yii\web\View $this */
/** @var array $shops */
/** @var integer $shopId */
/** @var array $products */

use yii\helpers\Html;

$this->title = 'Shop';
$this->params['breadcrumbs'][] = $this->title;
$n = 1;
?>
<div class="site-shop">
    <div class="row">
        <div class="col-3">
            <div class="list-group">
                <?foreach ($shops as $s):?>
                    <a href="<?=\yii\helpers\Url::to(['/site/shop', 'id' => $s['id']])?>"
                            class="list-group-item list-group-item-action <?=($n==$shopId?'active':'')?>"
                            <?=($n==0?'aria-current="true"':'')?>>
                        <?=$s['name'];?>
                    </a>
                <?
                $n++;
                endforeach;?>
            </div>
        </div>
        <div class="col-9">
            <div class="row row-cols-1 row-cols-md-2 g-4">
                <?foreach ($products as $p):?>
                <div class="col">
                    <div class="card">
                        <img src="/images/p<?=$p['image']?>.png" class="card-img-top" alt="<?=$p['name']?>">
                        <div class="card-body">
                            <h5 class="card-title"><?=$p['name']?></h5>
                            <p class="card-text"><?=str_replace("\r\n",'<br>',$p['description'])?></p>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-5">
                                    <button data-add-product="<?=$p['id']?>"
                                            class="btn  <?=(in_array($p['id'], $cart)?'btn-secondary':'btn-primary')?>">
                                        Add to cart
                                    </button>
                                </div>
                                <div class="col-7">
                                    <p class="text-end">Price: <b><?=$p['price']?></b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?endforeach;?>
            </div>
        </div>
    </div>
</div>
