<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property int|null $orderid
 * @property int|null $productid
 * @property int|null $quantity
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'orderid', 'productid', 'quantity'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'orderid' => 'Orderid',
            'productid' => 'Productid',
            'quantity' => 'Quantity',
        ];
    }
}
